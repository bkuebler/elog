/********************************************************************

   Name:         elog-version.h
   Created by:   Stefan Ritt

   Contents:     Version file for all ELOG programs

   $Id: elog-version.h 1 2012-06-10 12:56:11Z ritt $

\********************************************************************/

/* Version of ELOG */
#define VERSION "2.9.2"

/* ELOG identification */
static const char ELOGID[] = "elogd " VERSION " built " __DATE__ ", " __TIME__;
